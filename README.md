# Mirror Monitor

Monitor the status of known mirrors of f-droid.org.



## Active Mirrors

* https://f-droid.org/repo
* rsync://mirror.f-droid.org/
* https://fdroid.tetaneutral.net/fdroid/repo
* https://mirror.cyberbits.eu/fdroid/repo
* https://bubu1.eu/fdroid/repo
* https://fdroid.swedneck.xyz/fdroid/repo
* https://ftp.fau.de/fdroid/repo
* http://fauftpffbmvh3p4h.onion/fdroid/repo
* https://ftp.osuosl.org/pub/fdroid/repo
* https://mirror.scd31.com/fdroid/repo
* https://fdroid.fi-do.io/fdroid/repo
* https://plug-mirror.rcac.purdue.edu/fdroid/repo
* https://mirrors.tuna.tsinghua.edu.cn/fdroid/repo
* https://mirrors.nju.edu.cn/fdroid/repo
* https://mirror.kumi.systems/fdroid/repo
* https://ftp.lysator.liu.se/pub/fdroid/repo
* http://lysator7eknrfl47rlyxvgeamrv7ucefgrrlhk7rouv3sna25asetwid.onion/pub/fdroid/repo
* https://mirror.librelabucm.org/fdroid/repo/
* http://mirror.librelablmozifzc.onion/fdroid/repo/
* https://fdroid-mirror.calyxinstitute.org/fdroid/repo
* https://mirrors.dotsrc.org/fdroid/repo/
